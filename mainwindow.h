#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "pjas.h"
#include <QMainWindow>
#include <QtCore>
#include <QtNetwork>
#include <QBrush>
#include <QPen>
#include "ui_mainwindow.h"

//TODO rita �ven ut m�jliga drag!
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
	MainWindow(QWidget * parent = 0);
	void paintEvent(QPaintEvent* event);
	// resizeEvent....
   
	
	void gameEngine();
	void setStarter(); // set up for start.
	void mousePressEvent(QMouseEvent* event);
	bool askIfPromote(bool other_promoted); // fr�gar om vi skall promota
	
public slots:
	void gameOver();
	void nextPlayer(); 
	void newPlayerConnected();
	void ConnectedToServer();
	void readMove();
	void showPlayerTurn();
signals:
	void nextPlayerSIG();
	void showPlayerTurnSIG();

protected:
	void resizeEvent(QResizeEvent* event);
private:
	int datasize;
	
	Ui::MainWindow ui;
	bool did_promote;
	
	QList<Pjas> pjaser;
	bool checkMove(int player, int x,int y,int x2,int y2,bool checkschack); //kolla om gilltigt drag. 
	// Spelare player vill g� fr�n x,y till x2,y2. checkschack s�ger om vi skall kolla ifall
	// spelaren hamnar i schack. Det skall vi ej g�ra n�r vi anv�nder checkMove fr�n isSchacked
	
	void doMove(); // do the move
	// TODO funktion f�r att kolla om schack...
	int schack; // 0 = ingen �r i schack, -1 = du �r i schack, 1 = den andra �r i schack
    
	QList<QImage> bilder;
	int plan[300]; //0-80 = spelplan resten �r f�r att rita pj�ser utanf�r planen       Spelplanen �vers�tts till x*9+y i arrayen
	
	QBrush background;
	QBrush black;
	int clX,clY; // last clicked X,Y positions
	int stopx,stopy; // the end positions 
	int winner;  // the winner of the game
	bool ignoreClick; // should we ignore clicks?
	QPen blackp;
	void doDrop(int current,int clX,int clY,int stopx,int stopy); // g�r dropp om det inte leder till schack
	
	int schackadspelare; // vem �r schackad?
	QList<int> schackare; // vilka schackar? (mest f�r att rita)
	
	QPen redp;
	int sX,sY; // size of rectangle on gameboad. hela spelplanen �r 9*sX  resp 9*sY
	void reloadPictures(); //loads, or reloads pictures after scaling!
	int current; // current player, -1 = du, som startar d�r nere, 1 = den andre som startar d�r uppe
	
	bool isSchacked(int player,bool setparameters); // returnerar true omm spelaren player st�r i schack. setparameters ber�ttar om vi skall s�tta schackadspelare, schackare eller ej.
	
	
	QHostAddress serverIP;
	quint16 serverPort;
	QTcpServer* server;
	QTcpSocket* client;
	QTcpSocket* socket;
	bool isServer;
};

#endif
