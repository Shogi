#include "pjas.h"

/* typer	
	0 = bonde
	1 = lans
	2 = springare
	3 = l�pare
	4 = torn
	5 = silver
	6 = guld
	7 = kung
	8 = prmotoed bonde
	9 = prmotoed lans
	10 = prmotoed springare
	11 = prmotoed l�pare
	12 = prmotoed torn
	13 = promoted silver
	
	*/
Pjas::Pjas(int type)
{
	promoted = false;
	ispromotable = true;
	typ = type;
	isking = false;
	owner = 0;
	outside = false;
	xpos=-1;
	ypos=-1;
	if (typ == 6 || typ==7)
	{
		ispromotable = false;
		if (typ == 7)
			isking = true;
	}
}

int Pjas::picture()
{
	int Cowner;
	if (owner==-1)
		Cowner=0;
	else
		Cowner=1;
	if (typ ==0) 			return Cowner*14+0;
	else if (typ == 1)		return Cowner*14+1;
	else if (typ == 2)		return Cowner*14+2;
	else if (typ == 3)		return Cowner*14+3;
	else if (typ == 4)		return Cowner*14+4;
	else if (typ==5)		return Cowner*14+5;
	else if (typ==6)		return Cowner*14+6;
	else if (typ==7)		return Cowner*14+7;
	else if (typ==8)		return Cowner*14+8;
	else if (typ==9)		return Cowner*14+9;
	else if (typ==10)		return Cowner*14+10;
	else if (typ==11)		return Cowner*14+11
	else if (typ==12)		return Cowner*14+12;
	else 		return Cowner*14+13;
}
