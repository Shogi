#include <QtGui>
#include "pjas.h"
#include <cmath>
#include <cstdlib>
#include "mainwindow.h"

// -1 om x <0 1 om x>0 och 0 annars
int sgn(int x)
{
	if (x>0)
		return 1;
	else if (x<0)
		return -1;
	else 
		return 0;
}

int min(int a,int b)
{
	if (a>b)
		return b;
	else 
		return a;
}

int max(int a,int b)
{
	if (a>b)
		return a;
	else 
		return b;
}

MainWindow::MainWindow(QWidget *parent)
   : QMainWindow(parent, 0)
{
	datasize=0;
	
	connect(this,SIGNAL(nextPlayerSIG()),this,SLOT(nextPlayer()));
	current = -1;
    ui.setupUi(this);
    int i;
	// skapa pj�ser
	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));
	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));
	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));
	pjaser.push_back(Pjas(1));	pjaser.push_back(Pjas(1));	pjaser.push_back(Pjas(2));
	pjaser.push_back(Pjas(2));	pjaser.push_back(Pjas(3));	pjaser.push_back(Pjas(4));
	pjaser.push_back(Pjas(5));	pjaser.push_back(Pjas(5));	pjaser.push_back(Pjas(6));
	pjaser.push_back(Pjas(6));	pjaser.push_back(Pjas(7));
	
	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));
	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));
	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));	pjaser.push_back(Pjas(0));	
	pjaser.push_back(Pjas(1));	pjaser.push_back(Pjas(1));	pjaser.push_back(Pjas(2));
	pjaser.push_back(Pjas(2));	pjaser.push_back(Pjas(3));	pjaser.push_back(Pjas(4));
	pjaser.push_back(Pjas(5));	pjaser.push_back(Pjas(5));	pjaser.push_back(Pjas(6));
	pjaser.push_back(Pjas(6));	pjaser.push_back(Pjas(7));
	for (i=20; i<40;i++)
		pjaser[i].owner=1;
	for (i=0;i<300;i++)
		plan[i] = -1;
	
	background = QBrush(Qt::green);
	black = QBrush(Qt::black);
	blackp = QPen(Qt::black);
	redp = QPen(Qt::red);
	redp.setWidth(3);
	sX=100;
	sY=100; // 1 b�rjar d�r uppe... -1 d�r nere
	pjaser[ 0].xpos=0; pjaser[ 0].ypos=6; pjaser[ 0].owner=-1;
	pjaser[ 1].xpos=1; pjaser[ 1].ypos=6; pjaser[ 1].owner=-1;
	pjaser[ 2].xpos=2; pjaser[ 2].ypos=6; pjaser[ 2].owner=-1;
	pjaser[ 3].xpos=3; pjaser[ 3].ypos=6; pjaser[ 3].owner=-1;
	pjaser[ 4].xpos=4; pjaser[ 4].ypos=6; pjaser[ 4].owner=-1;
	pjaser[ 5].xpos=5; pjaser[ 5].ypos=6; pjaser[ 5].owner=-1;
	pjaser[ 6].xpos=6; pjaser[ 6].ypos=6; pjaser[ 6].owner=-1;
	pjaser[ 7].xpos=7; pjaser[ 7].ypos=6; pjaser[ 7].owner=-1;
	pjaser[ 8].xpos=8; pjaser[ 8].ypos=6; pjaser[ 8].owner=-1;
	pjaser[ 9].xpos=0; pjaser[ 9].ypos=8; pjaser[ 9].owner=-1;
	pjaser[10].xpos=8; pjaser[10].ypos=8; pjaser[10].owner=-1;
	pjaser[11].xpos=1; pjaser[11].ypos=8; pjaser[11].owner=-1;
	pjaser[12].xpos=7; pjaser[12].ypos=8; pjaser[12].owner=-1;
	pjaser[13].xpos=1; pjaser[13].ypos=7; pjaser[13].owner=-1;
	pjaser[14].xpos=7; pjaser[14].ypos=7; pjaser[14].owner=-1;
	pjaser[15].xpos=2; pjaser[15].ypos=8; pjaser[15].owner=-1;
	pjaser[16].xpos=6; pjaser[16].ypos=8; pjaser[16].owner=-1;
	pjaser[17].xpos=3; pjaser[17].ypos=8; pjaser[17].owner=-1;
	pjaser[18].xpos=5; pjaser[18].ypos=8; pjaser[18].owner=-1;
	pjaser[19].xpos=4; pjaser[19].ypos=8; pjaser[19].owner=-1;
	
	pjaser[20].xpos=0; pjaser[20].ypos=2; pjaser[20].owner=1;
	pjaser[21].xpos=1; pjaser[21].ypos=2; pjaser[21].owner=1;
	pjaser[22].xpos=2; pjaser[22].ypos=2; pjaser[22].owner=1;
	pjaser[23].xpos=3; pjaser[23].ypos=2; pjaser[23].owner=1;
	pjaser[24].xpos=4; pjaser[24].ypos=2; pjaser[24].owner=1;
	pjaser[25].xpos=5; pjaser[25].ypos=2; pjaser[25].owner=1;
	pjaser[26].xpos=6; pjaser[26].ypos=2; pjaser[26].owner=1;
	pjaser[27].xpos=7; pjaser[27].ypos=2; pjaser[27].owner=1;
	pjaser[28].xpos=8; pjaser[28].ypos=2; pjaser[28].owner=1;
	pjaser[29].xpos=0; pjaser[29].ypos=0; pjaser[29].owner=1;
	pjaser[30].xpos=8; pjaser[30].ypos=0; pjaser[30].owner=1;
	pjaser[31].xpos=1; pjaser[31].ypos=0; pjaser[31].owner=1;
	pjaser[32].xpos=7; pjaser[32].ypos=0; pjaser[32].owner=1;
	pjaser[33].xpos=7; pjaser[33].ypos=1; pjaser[33].owner=1;
	pjaser[34].xpos=1; pjaser[34].ypos=1; pjaser[34].owner=1;
	pjaser[35].xpos=2; pjaser[35].ypos=0; pjaser[35].owner=1;
	pjaser[36].xpos=6; pjaser[36].ypos=0; pjaser[36].owner=1;
	pjaser[37].xpos=3; pjaser[37].ypos=0; pjaser[37].owner=1;
	pjaser[38].xpos=5; pjaser[38].ypos=0; pjaser[38].owner=1;
	pjaser[39].xpos=4; pjaser[39].ypos=0; pjaser[39].owner=1;
	for (i=0;i<40;i++)
		plan[pjaser[i].xpos*9+pjaser[i].ypos]=i;
	
	// Load pictures, �ven de upp-och-ner-v�nda
	bilder.push_back(QImage("bonde.png"));	
	bilder.push_back(QImage("lans.png"));		
	bilder.push_back(QImage("hast.png"));		
	bilder.push_back(QImage("lopare.png"));
	bilder.push_back(QImage("torn.png"));		
	bilder.push_back(QImage("silver.png"));
	bilder.push_back(QImage("guld.png"));		
	bilder.push_back(QImage("kung.png"));		
	bilder.push_back(QImage("bondeP.png"));
	bilder.push_back(QImage("lansP.png"));
	bilder.push_back(QImage("hastP.png"));
	bilder.push_back(QImage("lopareP.png"));
	bilder.push_back(QImage("tornP.png"));
	bilder.push_back(QImage("silverP.png"));

	bilder.push_back(QImage("Ubonde.png"));	
	bilder.push_back(QImage("Ulans.png"));		
	bilder.push_back(QImage("Uhast.png"));		
	bilder.push_back(QImage("Ulopare.png"));
	bilder.push_back(QImage("Utorn.png"));		
	bilder.push_back(QImage("Usilver.png"));
	bilder.push_back(QImage("Uguld.png"));		
	bilder.push_back(QImage("Ukung.png"));		
	bilder.push_back(QImage("UbondeP.png"));
	bilder.push_back(QImage("UlansP.png"));
	bilder.push_back(QImage("UhastP.png"));
	bilder.push_back(QImage("UlopareP.png"));
	bilder.push_back(QImage("UtornP.png"));
	bilder.push_back(QImage("UsilverP.png"));
	
	
	/* typer	
	0 = bonde
	1 = lans
	2 = springare
	3 = l�pare
	4 = torn
	5 = silver
	6 = guld
	7 = kung
	8 = prmotoed bonde
	9 = prmotoed lans
	10 = prmotoed springare
	11 = prmotoed l�pare
	12 = prmotoed torn
	13 = promoted silver
	
	*/
	connect(this,SIGNAL(showPlayerTurnSIG()),this,SLOT(showPlayerTurn()));
	
	
	QString str;
	serverPort = 31416;
	
	if (QMessageBox::Yes == QMessageBox::question(this,"�r du server?","Skall du vara server eller ej?",QMessageBox::Yes |QMessageBox::No))
	{
		server = new QTcpServer(this);
		QList<QHostAddress> ipnr = QNetworkInterface::allAddresses();
		for (i=0;i<ipnr.size();i++)
		{
			if (ipnr[i] != QHostAddress::LocalHost)
				serverIP = ipnr[i];
		}
		str = QInputDialog::getText(this,"�r detta ditt IP?","�r detta ditt IP nummer?",QLineEdit::Normal,serverIP.toString());
		server->listen(serverIP,serverPort);
		connect(server,SIGNAL(newConnection()),this,SLOT(newPlayerConnected()));
		isServer=true;
	}		
	else
	{
		str = QInputDialog::getText(this,"Ange IP till din motst�ndares dator","Ange IP till din motst�ndares dator");
		serverIP = QHostAddress(str);
		socket = new QTcpSocket(this);
		socket->connectToHost(serverIP,serverPort);
		connect(socket,SIGNAL(connected ()),this,SLOT(ConnectedToServer()));
		isServer = false;
		connect(socket, SIGNAL(readyRead()), this, SLOT(readMove()));
	}
	
	

	setStarter();
	showPlayerTurn();
	reloadPictures();
}
void MainWindow::ConnectedToServer()
{
	qDebug()<<"Connectade till server";
}

void MainWindow::newPlayerConnected()
{
	qDebug()<<"n�gon connnectade till mig. Ah! Det �r min gamla nemesis.";
	client = server->nextPendingConnection();
	connect(client, SIGNAL(readyRead()), this, SLOT(readMove()));
}

void MainWindow::showPlayerTurn()
{
	if (current==1)
		ui.vemstur->setText("EJ din tur");
	else
		ui.vemstur->setText("Din tur");
}

void MainWindow::reloadPictures()
{
	bilder.clear();
	bilder.push_back(QImage("bonde.png"));	
	bilder.push_back(QImage("lans.png"));		
	bilder.push_back(QImage("hast.png"));		
	bilder.push_back(QImage("lopare.png"));
	bilder.push_back(QImage("torn.png"));		
	bilder.push_back(QImage("silver.png"));
	bilder.push_back(QImage("guld.png"));		
	bilder.push_back(QImage("kung.png"));		
	bilder.push_back(QImage("bondeP.png"));
	bilder.push_back(QImage("lopareP.png"));
	bilder.push_back(QImage("tornP.png"));
	
	bilder.push_back(QImage("Ubonde.png"));		
	bilder.push_back(QImage("Ulans.png"));		
	bilder.push_back(QImage("Uhast.png"));		
	bilder.push_back(QImage("Ulopare.png"));	
	bilder.push_back(QImage("Utorn.png"));		
	bilder.push_back(QImage("Usilver.png"));	
	bilder.push_back(QImage("Uguld.png"));		
	bilder.push_back(QImage("Ukung.png"));		
	bilder.push_back(QImage("UbondeP.png"));	
	bilder.push_back(QImage("UlopareP.png"));
	bilder.push_back(QImage("UtornP.png"));
	for (int i=0; i<bilder.size();i++)
	{
		bilder[i] = bilder[i].scaled(sX,sY);
	}
}

void MainWindow::paintEvent(QPaintEvent * parent)
{
	qDebug()<<"B�rja rita...";
	QPainter p(this);
	p.fillRect(0,0,sX*9,sY*9,background);
	int x,y,i;
	
	
	for (i=0;i<40;i++)
	{
		if (pjaser[i].outside)
		{	// rita utanf�r
			p.drawImage(pjaser[i].xpos*sX, pjaser[i].ypos*sY,bilder[pjaser[i].picture()]);
		}
		else
		{	// rita p� planen
			p.drawImage(pjaser[i].xpos*sX, pjaser[i].ypos*sY,bilder[pjaser[i].picture()]);
		}
	}
	p.setPen(blackp);
	for (x=0;x<=9;x++)
		for (y=0;y<=9;y++)
		{
			p.drawLine(x*sX,0,x*sX,y*sY);
			p.drawLine(0,y*sY,x*sX,y*sY);
		}
	if (clX != -1 && clY != -1)
	{
		p.setPen(redp);
		p.drawRect(clX*sX,clY*sY,sX,sY);
	}
	
	p.setPen(redp);
	int kung;
	if (schackadspelare == -1)
	{
		kung = 19;
	}
	else
	{
		kung = 39;
	}
	for (i=0;i<schackare.size();i++)
	{
		x=pjaser[schackare[i]].xpos;
		y=pjaser[schackare[i]].ypos;
		p.setPen(redp);
		qDebug()<<"rita schack : "<<x<<y;
		p.drawLine(x*sX+sX/2,y*sY+sY/2,pjaser[kung].xpos*sX+sX/2,pjaser[kung].ypos*sY+sY/2);
	}
	qDebug()<<".....sluta rita";
}

void MainWindow::gameOver()
{
	if (winner ==-1)
		QMessageBox::information(this,"Spelet slut","Datorn vann");
	else 
		QMessageBox::information(this,"Spelet slut","Du vann");
	exit(0);
}

void MainWindow::setStarter()
{
	if (isServer)
	{
		current = -1; // server b�rjar alltid :-D
		ignoreClick=false;
		// TODO Fr�ga om handikapp m.m.
	}
	else
	{
		current = 1;
		ignoreClick=true;
	}
	schack=0;
	clX= -1;
	winner =0;
	clY= -1;
	schackadspelare=0;
}


void MainWindow::mousePressEvent(QMouseEvent* event)
{
	int pos;
	int typ;
	int a,b;
	if (!ignoreClick)
	{
		int xpos = event->x();
		int ypos = event->y();
		xpos = xpos / sX;
		ypos = ypos / sY;
		if (xpos>=0 && xpos<=8 && ypos>=0 && ypos<=8)
		{
			if (clX == -1)   // inget klickat tidigare och inte tom ruta
			{
				if (plan[xpos*9+ypos] != -1  &&  pjaser[plan[xpos*9+ypos]].owner == current)
				{
					clX = xpos;
					clY = ypos;
					//ui.move->setVisible(false);
				}
			}
			else // has previously clicked figure
			{
				stopx = xpos;
				stopy = ypos;
				if (stopx==clX && stopy==clY)
				{
					clX = -1;
					clY = -1; // "unselect" the last click
				}
				else 
				{ 
					if (clX>9) // droppa pj�s
					{
						pos = stopx*9+stopy;
						typ = pjaser[plan[clX*9+clY]].typ;
						
						if (plan[pos] == -1)
						{
							if ( typ>2 || ( (typ == 0 || typ == 1) && clY != 4+4*current) ||
								(typ == 2 && clY != 4+3*current) )
							{
								b=0; 
								if (typ==0) 
									for (a=0;a<9;a++)
										if (plan[stopx*9+a]!= -1 && pjaser[plan[stopx*9+a]].typ==0 && pjaser[plan[stopx*9+a]].owner==current)
											b++;
								if (b==0)
								{
									doDrop(current,clX,clY,stopx,stopy);
								}
							}
						}
					}
					else
					{
						if (plan[stopx*9+stopy] == -1) // tomt h�r
						{
							if ( checkMove(current,clX,clY,stopx,stopy,true) )
							{
								doMove();
							}
						}
						else if (pjaser[plan[stopx*9+stopy]].owner == current)
						{
							clX = xpos; // byt pj�s
							clY = ypos;
						}
						else  // C H A R G E !!!
						{
							if (checkMove(current,clX,clY,stopx,stopy,true))
							{
								doMove();
							}
						}
					}
				}
			}
		}
		else // klickade utanf�r spelplanen
		{
			pos = xpos*9 +ypos;
			if (pos<300 && plan[pos] != -1 && pjaser[plan[pos]].owner == current)
			{
				//flytta in pj�sen om det n�sta g�ng klickas p� ledig ruta
				clX = xpos;
				clY = ypos;
			}
			else{
				clX = -1;
				clY = -1;
			}
		}
		update();
	}
}

// player �r oftast current som skickas. checkschack s�ger om vi skall kolla om det �r schack
bool MainWindow::checkMove(int player, int x1,int y1,int x2,int y2,bool checkschack) //kolla om gilltigt drag
{
//	qDebug()<<"Spelare "<<player<<" vill flytta fr�n "<<x1<<y1<<"  till "<<x2<<y2;
	int pjas = x1*9+y1;
	int typ = pjaser[plan[pjas]].typ;
	int dx,dy,y,x;
	int fiende = plan[x2*9+y2]; // id p� den som st�r p� m�l rutan
	bool dragOK=true; // om draget, rent flyttningsm�ssigt �r ok, kollar ej om schack hot avv�rjs
	
	if (fiende == -1)
		dragOK = true; // beh�ver ej kolla om tomma rutor
	else if (pjaser[fiende].owner == player) 
		return false; // d�r fanns en v�nskaplig pj�s

	// OBS! dragOK m�ste ha true v�rde n�r vi g�r in h�r...
	// kolla om motst�ndaren �r i schack, d� �r partiet vunnet!
	// kolla om jag �r i schack
	//  g�r en massa...
	
	// Kolla om draget �r giltigt
	
	dx = x2 - x1;
	dy = y2 - y1;
	
	// Varje if-sats skall s�tta true p� dragOK variabeln eller returnera false direkt
	if (typ == 2) // h�st
	{
		dragOK = (	(abs(x1-x2)==1) && (y2 == y1+2*player)); // okflytt flytt+2 f�r uppe, -2 f�r nere
	}
	else	if (typ == 0) // bonde
	{
		dragOK = (dx==0) && (dy == player); // smidigt med -1 och 1 parametrar f�r spelaren
	}
	else if (typ == 1) // lans
	{
		if (dx!=0) return false;
		for (y=y1+player; dragOK && y!=y2; y=y+player)
		{
			dragOK = plan[x1*9 + y] == -1;
		}
	}
	else if (typ == 3 ) // l�pare
	{
		if (abs(dx) != abs(dy))
			return false;
		dx = sgn(dx);
		dy = sgn(dy);
		x=x1+dx;
		y=y1+dy;
		while (x!=x2 && dragOK )
		{
			dragOK = plan[x*9+y] == -1;
			x = x + dx;
			y = y + dy;
		}
	}
	else if (typ == 4) // torn
	{
		if (dy == 0)
		{
			dx = sgn(dx);
			x=x1+dx;
			while (x!=x2 && dragOK )
			{
				dragOK = plan[x*9+y1] == -1;
				x = x + dx;
			}
		}
		else if (dx == 0)
		{
			dy = sgn(dy);
			y=y1+dy;
			while (y!=y2 && dragOK )
			{
				dragOK = plan[x1*9+y] == -1;
				y = y + dy;
			}
		}
		else
			return false;
	}
	else if (typ == 5) // silver
	{
		if (abs(dx)>1 ||  abs(dy)>1 ||  dy == 0 ||   (dx==0 && dy == -player) )
			return false;
		// resten �r ok if (abs(dy*dx) == 1 || (dx==0 && dy=sgn(current) )  )
		// dragOK = true;
	}
	else if (typ == 6 || typ == 8 || typ == 9 || typ == 10 || typ == 13) // guld
	{
		if (abs(dx)>1 || abs(dy)>1 || ( abs(dx)==1 && dy == -player) )
			return false;
		// resten �r ok att g�ra.
	}
	else if (typ == 7) // knugen
	{
		if (abs(dx)>1 || abs(dy)>1  ||  
		   		abs(pjaser[19].xpos-pjaser[39].xpos)<2 &&
				abs(pjaser[19].ypos-pjaser[39].ypos)<2 )  // kolla s� inte f�r n�ra andra kungen)
			return false; 
		// allt annat inneb�r att kungen flyttar 1 steg eftersom vi redan har gallrat bort 
		// m�jligheten att st� kvar p� samma plats
	}
	else if (typ == 11) //promoted l�pare
	{
		if (abs(dx)>1 || abs(dy)>1)
		{
			if (abs(dx) != abs(dy))
				return false;
			dx = sgn(dx);
			dy = sgn(dy);
			x=x1+dx;
			y=y1+dy;
			while (x!=x1 && dragOK )
			{
				dragOK = plan[x*9+y] == -1;
				x = x + dx;
				y = y + dy;
			}
		}
		// else, annars �r det ett drag som godk�nns f�r det �r precis som f�r en kung
	}
	else if (typ == 12) // promoted torn
	{
		if (abs(dx)>1 || abs(dy)>1 )
		{
			if (dy == 0)
			{
				dx = sgn(dx);
				x=x1+dx;
				while (x!=x2 && dragOK )
				{
					dragOK = plan[x*9+y1] == -1;
					x = x + dx;
				}
			}
			else if (dx == 0)
			{
				dy = sgn(dy);
				y=y1+dy;
				while (y!=y2 && dragOK )
				{
					dragOK = plan[x1*9+y] == -1;
					y = y + dy;
				}
			}
			else
				return false;
		}//else, ok, eftersom det �r som ett kung drag.
	}
	else
		dragOK = false;
	
	// snabb koll...
	if (!dragOK) 
	{
		//qDebug()<<"Draget �r INTE ok, g�r om, g�r r�tt!";
		return dragOK;
	}

	// kolla om schackhot avv�rjt m.m. TODO  st�ng av ritningen samtidigt?
	// First, save some stuff for undo
	// Make a pseudomove...
	// pjas = x1*9+y1 piece
	if (checkschack)
	{
		int fplats = x2*9+y2;
		int fnummer = plan[fplats];
		int plats = x1*9+y1;
		int nummer = plan[plats];
		if (fnummer != -1)
		{	
			pjaser[fnummer].owner = current;
			pjaser[fnummer].outside= true;
		}
		Pjas old = pjaser[nummer]; // kopiera �ver all data
		
		plan[fplats] = nummer;
		plan[plats] = -1;
		pjaser[nummer].xpos = x2;
		pjaser[nummer].ypos = y2;
		
		if (isSchacked(current,false))
		{
			dragOK=false;
			ui.message->setText("Du f�r ej flytta s� f�r d� blir du schackad");
		}
		else
		{
			dragOK=true;
			ui.message->setText("Draget �r okej");
		}
		
		// undo move
		if (fnummer != -1)
		{
			pjaser[fnummer].owner = -current;
			pjaser[fnummer].outside = false;
		}
		pjaser[nummer] = old;
		plan[fplats] =fnummer;
		plan[plats] = nummer;
	}
	return dragOK; 
}


/*
 Unders�k om spelaren player st�r i schack.
S�tter v�rden p� schackadspelare och schackare listan om n�gon �r schackad om setParameters
�r true
 returnerar true om*/
bool MainWindow::isSchacked(int player,bool setParameters)
{
	// Kolla om n�gon av most�ndar pj�serna kan n� kungen som tillh�r player
	//qDebug()<<"\t\tLetar schack";
	int kung,kx,ky,i;
	if (player == -1)
		kung=19;
	else
		kung=39;
	kx = pjaser[kung].xpos;
	ky = pjaser[kung].ypos;
	bool schack=false; // om schack
	if (setParameters)
	{
		schackare.clear(); // ta bort alla tidigare schackare
		schackadspelare = 0;
	}
	for (i=0;i<39;i++)
	{
		if ( (i != 19) && (pjaser[i].owner != player) && (!pjaser[i].outside) )// pj�s i tillh�r den andra
		{
	//		qDebug()<<"\t\tF�rs�k med pj�s  "<<i<<" i position" <<pjaser[i].xpos<<pjaser[i].ypos<<" kung i "<<kx<<ky;
			if ( checkMove(-player,pjaser[i].xpos,pjaser[i].ypos,kx,ky,false) )
			{ // vi kunde n� kungen med pj�s i
				schack = true;
				if (setParameters)
				{
					schackadspelare = player;
					schackare.push_back(i);
				}
			}
		}
	}
	//qDebug()<<"\t\tSlutat leta schack";
	return schack;
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
	sX = (int)((width()-1)/14);
	sY = (int)((height()-1)/9);
	if (sX<sY)
		sY=sX;
	else
		sX=sY;
	reloadPictures();
}

void MainWindow::doMove() // do the move
{
	int x,y,i;
	int piece = plan[clX*9+clY]; //pj�sen d�r
	if (plan[stopx*9+stopy] != -1)
	{	// ta pj�s, redan kollat s� att detta �r m�jligt.
		int tagen = plan[stopx*9+stopy];
		// hitta ledig plats
		x=10; y=4-4*current; i=0;
		while (plan[x*9+y]!= -1)
		{
			i++;
			x=10+(i%4);
			if (i>0 && i%4==0)
				y=y+current;
		}
		pjaser[tagen].xpos = x;
		pjaser[tagen].ypos = y;
		plan[x*9+y]=tagen;
		pjaser[tagen].owner = current;
		pjaser[tagen].promoted = false;
		pjaser[tagen].outside= true;
		if (pjaser[tagen].typ>7)
			pjaser[tagen].typ -= 8; // �ndra ner typ till l�gre grad
	}
	plan[stopx*9 +stopy] = piece;
	pjaser[piece].xpos = stopx;
	pjaser[piece].ypos = stopy;
	
	plan[clX*9+clY] = -1;
	// Kolla om vi kan promota
	if ( ((current==-1 && stopy<=2) || (current==1 && stopy>=6) )  && pjaser[piece].typ<6  && askIfPromote(did_promote) )
	{
		pjaser[piece].typ += 8;
		did_promote=true;
	} 
	else
		did_promote=false;
	update();
	// Just nu �r det current som g�r draget, kolla om -current �r schackad
	
	if (isSchacked(-current,true)) // s�tt schack flaggor
		if (current == 1)
			ui.message->setText("Du �r schackad");
		else
			ui.message->setText("Datorn �r schackad");
	else
		ui.message->setText("");
	
	emit nextPlayerSIG();
}

bool MainWindow::askIfPromote(bool other_promoted) //
{
	if (current==1)
	{
		return other_promoted;
	}
	else 
	{
		return (QMessageBox::question(this,"Vill du promota?","Vill du promota? Det brukar vara b�st att g�ra?",QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes) == QMessageBox::Yes);
	}
}


void MainWindow::doDrop(int current,int clX,int clY,int stopx,int stopy)
{
	int plats = clX*9+clY;
	int fplats = stopx*9+stopy;
	int nummer = plan[plats];
	Pjas old = pjaser[nummer];
	
	plan[fplats]=nummer;
	plan[plats]=-1;
	pjaser[nummer].outside=false;
	pjaser[nummer].promoted = false;
	pjaser[nummer].xpos=stopx;
	pjaser[nummer].ypos=stopy;
	if (isSchacked(current,false))
	{
		// draget hj�lpte inte, du �r fortfarande schackad
		pjaser[nummer]=old;
		plan[fplats]=-1;
		plan[plats]=nummer;
	}
	else 
	{
		isSchacked(-current,true); // kolla om den andra �r schackad
		did_promote=false;
		emit nextPlayerSIG();
	}
}

void MainWindow::readMove()
{
	QDataStream in;
	if (isServer)
	{
		in.setDevice(client);
		in.setVersion(QDataStream::Qt_4_0);
		if (client->bytesAvailable()<5*(int)sizeof(qint32))
			return;
	}
	else
	{
		in.setDevice(socket);
		in.setVersion(QDataStream::Qt_4_0);
		if (socket->bytesAvailable()<5*(int)sizeof(qint32))
			return;
	}
	
	
	int promote;
	in>>clX>>clY>>stopx>>stopy>>promote;
	did_promote = (promote!=0);
	qDebug()<<"Tog emot "<<clX<<clY<<stopx<<stopy<<promote;
	
	if (clX<9)
	{
		clY=8-clY;
		clX=8-clX; // spegelv�nt!
		stopy=8-stopy; // de �r ju upp och ner!
		stopx=8-stopx;
		doMove();
	} 
	else 
	{
		clY=8-clY;
		// r�r ej clX !!
		stopy=8-stopy; // de �r ju upp och ner!
		stopx=8-stopx;
		// Hur fan blir det med drop  och stopy???
		doDrop(current, clX,clY,stopx,stopy);
	}
}

void MainWindow::nextPlayer()
{
	update();
	
	if (current == -1) // Det var din tur
	{
		qDebug()<<"kom in in nextPlayer och det �r current = "<<current;
		ignoreClick=true;
		// f�rbered f�r att skicka data
		
		QByteArray block;
     	QDataStream out(&block, QIODevice::WriteOnly);
     	out.setVersion(QDataStream::Qt_4_0);
		out<<(qint32)clX<<(qint32)clY<<(qint32)stopx<<(qint32)stopy;
		
		if(did_promote)
			out<<(qint32)1;
		else
			out<<(qint32)0;
		
		// skicka data
		if (isServer)	
		{
			client->write(block);
		}
		else
		{
			socket->write(block);
		}
		qDebug()<<"skickade "<<clX<<clY<<stopx<<stopy<<"1 eller 0";
		current = -current;	
		clX = -1;
		clY = -1;	
		emit showPlayerTurnSIG();
	}
	else
	{
		ignoreClick=false; // det var den andres tur, nu �r det din tur
		current = -current;
		clX=-1;
		clY=-1;
		qDebug()<<"L�mnar nextPlayer() och p� denna dator �r det nu current = "<<current;
		emit showPlayerTurnSIG();
	}
	
}
