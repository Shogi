#include <QApplication>

#include "mainwindow.h"
#include <ctime>
#include <cstdlib>

int main(int argc, char *argv[])
{
	srand(time(0));
    QApplication app(argc, argv);
    MainWindow *MainForm = new MainWindow;
    //app.setMainWidget(MainForm);
    MainForm->show();
    return app.exec();
}
